# Qt Formula 1
A small application that consolidates essential information about Formula 1 in one place.
It uses Qt for the graphical interface and accesses an API.
The application displays the latest (or current) circuit being raced on, along with weather information.
It also includes a list of all circuits with their images, as well as details about the drivers.
Additionally, there is a calendar showing upcoming events, including dates and times of races.
Originally, my plan was to create an application that fetches real-time data such as speed, throttle, etc., for each driver, and displays them in graphs. However, these data refresh four times per second, and my computer could not handle such a high volume of data.

## Functionality
Nothing complicated - for displaying details of circuits or drivers, simply select one from the list. Similiary, for the calendar, dates with a red background indicate days when a session takes time (practice, quali, race, etc.).

## Roadmap
I'd like to implement more things in the future, like positions of drivers and the graphs, if I find a more efficient way or upgrade my computer :D

## API
This application uses the OpenF1 API to fetch data. It provides real-time and also historic data.
It isnt perfect; for example, positions of drivers are arranged by their number, and it doesnt match the actual data.
Visit https://openf1.org/?python#introduction or https://github.com/br-g/openf1.
